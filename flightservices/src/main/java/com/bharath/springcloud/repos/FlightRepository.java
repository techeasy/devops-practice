package com.bharath.springcloud.repos;

import com.bharath.springcloud.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightRepository extends JpaRepository<Flight, Long> {

}
